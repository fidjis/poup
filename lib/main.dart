import 'package:flutter/material.dart';
import 'package:poup/screens/home_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'poupEconomizando',
      theme: ThemeData(
        cardColor: Colors.blueGrey[400],
        primarySwatch: Colors.blue,
      ),
      home: HomeScreen(),
    );
  }
}