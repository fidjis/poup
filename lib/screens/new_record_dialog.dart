import 'package:flutter/material.dart';
import 'package:poup/util/sqf_helper.dart';

class NewRecordDialog extends StatefulWidget {

  @override
  _NewRecordDialogState createState() => _NewRecordDialogState();
}

var primaryColor = Colors.black;
var keypadColor = Colors.black;
typedef KeyCallBack = void Function(Key key);

class _NewRecordDialogState extends State<NewRecordDialog> {

  SQFHelper helper = SQFHelper();

  String _selectedCont = 'Carteira';
  List<Cont> conts = List<Cont>();

  Key _sevenKey = Key('seven');
  Key _eightKey = Key('eight');
  Key _nineKey = Key('nine');
  Key _fourKey = Key('four');
  Key _fiveKey = Key('five');
  Key _sixKey = Key('six');
  Key _oneKey = Key('one');
  Key _twoKey = Key('two');
  Key _threeKey = Key('three');
  Key _dotKey = Key('dot');
  Key _zeroKey = Key('zero');
  Key _clearKey = Key('clear');
  Key _allClearKey = Key('allclear');
  Key _equalsKey = Key('equals');

  TextEditingController _textEditingController;
  List _currentValues = List();
  double lastValue;
  bool savedLastValue = false;

  void initState() {
    super.initState();
    _textEditingController = TextEditingController();

    helper.getAllConts().then((list) {
      setState(() {
        try{
          conts = list;
        }catch(a){
          print(a.toString());
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: primaryColor,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Text('Novo Registro', style: TextStyle(color: Colors.white)),
        actions: [
          FlatButton(
            child: Text("SALVAR", style: TextStyle(color: Colors.white)),
            onPressed: ()  {
              String value = "";
              for(String f in _currentValues){
                value+= f;
              }
              Register newReg = Register(
                  value: value,
                  parcelasTotal: 2,
                  parcelasPagas: 1,
                  isSaldo: 1,
                  description: "Divida do mercado",
                  conta: _selectedCont
              );
              helper.saveRegister(
                newReg
              ).whenComplete((){

                helper.getAllRegister().then((list){
                  print("Tamanho no db ${list.length}");
                  print(list);
                });
                Navigator.of(context).pop(newReg);
              });

            },
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            height: 300.0,
            color: Colors.transparent,
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.blueGrey,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(40.0),
                        topRight: const Radius.circular(40.0)
                    )
                )
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.bottomRight,
                width: width,
                height: (height/100)*20,
                child: IgnorePointer(
                  child: TextField(
                    enabled: true,
                    autofocus: false,
                    controller: _textEditingController,
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      fontFamily: 'Avenir',
                      fontStyle: FontStyle.normal,
                      color: Colors.white,
                      fontSize: 60.0,
                    ),
                    decoration: InputDecoration.collapsed(
                        hintText: '0',
                        hintStyle: TextStyle(
                            color: Colors.white,
                            fontSize: 60.0
                        )
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text("Parcelas"),
                  DropdownButton<String>(
                    items: <String>['1', '2'].map((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    onChanged: (_) {},
                  ),
                  Container(
                    padding: EdgeInsets.all(5.0),
                    child: GestureDetector(
                      child: Row(children: <Widget>[
                        Text(_selectedCont, style: TextStyle(color: Colors.white, fontSize: 20.0)),
                        Icon(Icons.keyboard_arrow_down, color: Colors.white,)
                      ]),
                      onTap: _buildBottomSheetLocationCont,
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius:BorderRadius.only(
                            topLeft: Radius.circular(40.0)
                        )
                    ),
                    child: DropdownButton<String>(
                      items: <String>['Despeza', 'Saldo'].map((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (_) {},
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.all(5.0),
                color: Colors.black,
                child: TextField(
                  maxLines: 2,
                  style: TextStyle(
                    color: Colors.white
                  ),
                  decoration: InputDecoration(
                    hintStyle: TextStyle(
                      color: Colors.white
                    ),
                    border: InputBorder.none,
                    hintText: 'Descrição...',
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(30.0),
//                  color: Colors.white,
                  color: Colors.blueGrey[100],
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            buildKeyItem('7', _sevenKey),
                            buildKeyItem('8',_eightKey),
                            buildKeyItem('9',_nineKey),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            buildKeyItem('4',_fourKey),
                            buildKeyItem('5',_fiveKey),
                            buildKeyItem('6',_sixKey),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            buildKeyItem('1',_oneKey),
                            buildKeyItem('2',_twoKey),
                            buildKeyItem('3',_threeKey),
                          ],
                        ),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            buildKeyItem('.',_dotKey),
                            buildKeyItem('0',_zeroKey),
                            KeyItem(
                              key: _clearKey,
                              child: Icon(
                                Icons.backspace,
                                size: 40,
                                color: keypadColor,
                              ),
                              onKeyTap: onKeyTapped,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      )
    );
  }

  _buildBottomSheetLocationCont() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  IconButton(icon: Icon(Icons.add), onPressed: () async{
                    conts.add(await helper.saveCont(Cont("Conta Nova")));
                  })
                ],
              ),
              ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                itemCount: conts.length,
                itemBuilder: (BuildContext context, int index) => ListTile(
                  leading: Icon(Icons.videocam),
                  title:  Text(conts[index].contName),
                  onTap: () {
                    setState(() {
                      _selectedCont = conts[index].contName;
                      Navigator.pop(context);
                    });
                  },
                ),
              ),

            ],
          );
        });
  }

  KeyItem buildKeyItem(String val, Key key) {
    return KeyItem(
      key: key,
      child: Text(
        val,
        style: TextStyle(
          color: keypadColor,
          fontFamily: 'Avenir',
          fontStyle: FontStyle.normal,
          fontSize: 50.0,
        ),
      ),
      onKeyTap: onKeyTapped,
    );
  }

  void onKeyTapped(Key key) {
    if (savedLastValue == false && lastValue != null) {
      _currentValues.clear();
      savedLastValue = true;
    }
    setState(() {
      if (identical(_sevenKey, key)) {
        _currentValues.add('7');
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_eightKey, key)) {
        _currentValues.add('8');
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_nineKey, key)) {
        _currentValues.add('9');
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_fourKey, key)) {
        _currentValues.add('4');
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_fiveKey, key)) {
        _currentValues.add('5');
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_sixKey, key)) {
        _currentValues.add('6');
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_oneKey, key)) {
        _currentValues.add('1');
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_twoKey, key)) {
        _currentValues.add('2');
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_threeKey, key)) {
        _currentValues.add('3');
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_dotKey, key)) {
        if (!_currentValues.contains('.')) {
          _currentValues.add('.');
        }
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_zeroKey, key)) {
        _currentValues.add('0');
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_clearKey, key)) {
        print('Values :: $_currentValues');
        _currentValues.removeLast();
        _textEditingController.text = convertToString(_currentValues);
      } else if(identical(_allClearKey, key)) {
        _currentValues.clear();
        lastValue = null;
        savedLastValue = false;
        _textEditingController.clear();
      } else if (identical(_equalsKey, key)) {
        calculateValue();
        savedLastValue = false;
      }
    });
  }

  String convertToString(List values) {
    String val = '';
    print(_currentValues);
    for (int i = 0;i < values.length;i++) {
      val+=_currentValues[i];
    }
    return val;
  }

  void calculateValue() {

  }
}

class KeyItem extends StatelessWidget {

  final Widget child;
  final Key key;
  final KeyCallBack onKeyTap;

  KeyItem({@required this.child,this.key,this.onKeyTap});

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterial(context));
    return Expanded(
      child: Material(
        type: MaterialType.transparency,
        child: InkResponse(
          splashColor: primaryColor,
          highlightColor: Colors.white,
          onTap: () => onKeyTap(key),
          child: Container(
            //color: Colors.white,
            alignment: Alignment.center,
            child: child,
          ),
        ),
      ),
    );
  }
}
