import 'package:flutter/material.dart';
import 'package:poup/models/item_expansion_panel.dart';
import 'package:poup/screens/new_record_dialog.dart';
import 'package:poup/util/sqf_helper.dart';
import 'package:poup/util/util.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  SQFHelper helper = SQFHelper();
  List<Register> registers = List<Register>();
  String _selectedCont = 'Carteira';
  List<Cont> conts = List<Cont>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: ListTile(
            title: Text("poup",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold)),
            //              trailing: CircleAvatar(child: Icon(Icons.person, size: 40.0,),radius: 45,)
          ),
          backgroundColor: Colors.black,
          //            leading: Icon(Icons.attach_money, color:  Colors.white,),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
          backgroundColor: Colors.grey[400],
          onPressed: _openNewRecordDialog,
        ),
        body: ListView(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  gradient: LinearGradient(
                      colors: [
                        Color.fromARGB(255, 0, 0, 0),
                        Color.fromARGB(220, 0, 0, 0)
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight)
              ),
              child: Column(
                children: <Widget>[
                  Stack(
                    fit: StackFit.passthrough,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 95.0),
                        child: Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.blueGrey,
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: const Radius.circular(40.0),
                                      bottomRight: const Radius.circular(40.0))),
                              height: 200,
                              child: Container(),
                            ),
                            Container(
//                            color: Colors.black,
                              height: 70,
                              child: _buildProfileRow(),
                            )
                          ],
                        ),
                      ),
                      ClipPath(
                        child: FractionallySizedBox(
                          widthFactor: 1.0,
                          child: Container(
                            height: 150.0,
                            color: Colors.black,
                            child: Padding(
                                padding: EdgeInsets.only(
                                    top: 10.0, bottom: 0.0, left: 30.0, right: 0.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text("R\$200",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 50.0)),
                                    GestureDetector(
                                      child: Row(children: <Widget>[
                                        Text(_selectedCont,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 20.0)),
                                        Icon(
                                          Icons.keyboard_arrow_down,
                                          color: Colors.white,
                                        )
                                      ]),
                                      onTap: _buildBottomSheetLocationCont,
                                    )
                                  ],
                                )),
                          ),
                        ),
                        clipper: BottomWaveClipper(),
                      ),
                    ],
                  ),
                  _buildListCountsForDate()

                ],
              ),
            )
          ],
        )
    );
  }

  _openNewRecordDialog() async {
    Register newReg =
        await Navigator.of(context).push(new MaterialPageRoute<Register>(
            builder: (BuildContext context) {
              return NewRecordDialog();
            },
            fullscreenDialog: true));

    if (newReg != null) registers.add(newReg);
  }

  _buildBottomSheetLocationCont() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  IconButton(icon: Icon(Icons.add), onPressed: () async{
                    conts.add(await helper.saveCont(Cont("Conta Nova")));
                  })
                ],
              ),
              ListView.builder(
                physics: ClampingScrollPhysics(),
                shrinkWrap: true,
                itemCount: conts.length,
                itemBuilder: (BuildContext context, int index) => ListTile(
                      leading: Icon(Icons.credit_card),
                      title: Text(conts[index].contName),
                      onTap: () {
                        setState(() {
                          _selectedCont = conts[index].contName;
                          Navigator.pop(context);
                        });
                      },
                    ),
              ),
            ],
          );
        });
  }

  _buildProfileRow() {
    int _imageHeight = 16; //NAO SEI O VALOR, APENAS CHUTEI

    return new Padding(
      padding: new EdgeInsets.only(left: 16.0, top: _imageHeight / 2.5),
      child: new Row(
        children: <Widget>[
          new CircleAvatar(
            minRadius: 28.0,
            maxRadius: 28.0,
//            backgroundImage: new AssetImage('images/avatar.jpg'),
          ),
          new Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  'Ryan Barnes',
                  style: new TextStyle(
                      fontSize: 26.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w400),
                ),
                new Text(
                  'Product designer',
                  style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w300),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    helper.getAllRegister().then((list) {
      setState(() {
        registers = list;
      });
    });
    
    helper.getAllConts().then((list) {
      setState(() {
        try{
          conts = list;
        }catch(a){
          print("EEEEEEEEEEE" + a.toString());
        }
      });
    });
  }

  List<MyItem> _items = <MyItem>[
    new MyItem(header: 'header', body: 'body'),
    new MyItem(header: 'header', body: 'body'),
    new MyItem(header: 'header', body: 'body'),
    new MyItem(header: 'header', body: 'body'),
    new MyItem(header: 'header', body: 'body'),
  ];

  _buildListCountsForDate(){
    return ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: _items.length,
        itemBuilder: (BuildContext context, int index) => ItemExpansionPanel(registers)
    );
  }
}

class MyItem {
  MyItem({this.isExpanded: false, this.header, this.body});

  bool isExpanded;
  final String header;
  final String body;
}




