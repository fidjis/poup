import 'dart:async';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

final String contsTable = "contsTable";
final String contNameColumn = "contNameColumn";

final String registerTable = "registerTable";
final String idColumn = "idColumn";
final String valueColumn = "valueColumn";
final String isSaldoColumn = "isSaldoColumn";
final String parcelasTotalColumn = "parcelasTotalColumn";
final String parcelasPagasColumn = "parcelasPagasColumn";
final String contaColumn = "contaColumn";
final String descriptionColumn = "descriptionColumn";

class SQFHelper {

  static final SQFHelper _instance = SQFHelper.internal();

  factory SQFHelper() => _instance;

  SQFHelper.internal();

  Database _db;

  Future<Database> get db async {
    if(_db != null){
      return _db;
    } else {
      _db = await initDb();
      return _db;
    }
  }

  Future<Database> initDb() async {
    final databasesPath = await getDatabasesPath();
    final path = join(databasesPath, "poup.db");

    return await openDatabase(path, version: 1, onCreate: (Database db, int newerVersion) async {
      await db.execute(
          "CREATE TABLE $registerTable("
              "$idColumn INTEGER PRIMARY KEY, "
              "$valueColumn TEXT, "
              "$isSaldoColumn BIT,"
              "$parcelasTotalColumn INT, "
              "$parcelasPagasColumn INT, "
              "$contaColumn TEXT, "
              "$descriptionColumn TEXT);"
          "CREATE TABLE $contsTable("
              "$idColumn INTEGER PRIMARY KEY, "
              "$contNameColumn TEXT);"
      );
      await db.execute(
          "CREATE TABLE $contsTable("
              "$idColumn INTEGER PRIMARY KEY, "
              "$contNameColumn TEXT);"
      );
    });
  }

  Future<Cont> saveCont(Cont cont) async {
    Database database = await db;
    cont.id = await database.insert(contsTable, cont.toMap());
    return cont;
  }

  Future<List> getAllConts() async {
    Database database = await db;
    List listMap = await database.rawQuery("SELECT * FROM $contsTable");
    List<Cont> listCont = List();
    for(Map m in listMap){
      listCont.add(Cont.fromMap(m));
    }
    return listCont;
  }

  Future<Register> saveRegister(Register register) async {
    Database dbRegister = await db;
    register.id = await dbRegister.insert(registerTable, register.toMap());
    return register;
  }

  Future<Register> getRegister(int id) async {
    Database dbRegister = await db;
    List<Map> maps = await dbRegister.query(registerTable,
        columns: [idColumn, valueColumn, isSaldoColumn, parcelasTotalColumn, parcelasPagasColumn, contaColumn, descriptionColumn],
        where: "$idColumn = ?",
        whereArgs: [id]);
    if(maps.length > 0){
      return Register.fromMap(maps.first);
    } else {
      return null;
    }
  }

  Future<int> deleteRegister(int id) async {
    Database dbRegister = await db;
    return await dbRegister.delete(registerTable, where: "$idColumn = ?", whereArgs: [id]);
  }

  Future<int> updateRegister(Register register) async {
    Database dbRegister = await db;
    return await dbRegister.update(registerTable,
        register.toMap(),
        where: "$idColumn = ?",
        whereArgs: [register.id]);
  }

  Future<List> getAllRegister() async {
    Database dbRegister = await db;
    List listMap = await dbRegister.rawQuery("SELECT * FROM $registerTable");
    List<Register> listRegister = List();
    for(Map m in listMap){
      listRegister.add(Register.fromMap(m));
    }
    return listRegister;
  }

  Future<int> getNumber() async {
    Database dbRegister = await db;
    return Sqflite.firstIntValue(await dbRegister.rawQuery("SELECT COUNT(*) FROM $registerTable"));
  }

  Future close() async {
    Database dbRegister = await db;
    dbRegister.close();
  }

}

class Cont{

  int id;
  String contName;

  Cont(this.contName);

  Cont.fromMap(Map map){
    id = map[idColumn];
    contName = map[contNameColumn];
  }

  Map toMap() {
    Map<String, dynamic> map = {
      contNameColumn: contName,
    };
    if(id != null){
      map[idColumn] = id;
    }
    return map;
  }

}

class Register {

  int id;
  String value;
//  bool isSaldo;
  int isSaldo;
  int parcelasTotal;
  int parcelasPagas;
  String conta;
  String description;


  Register({this.value, this.isSaldo, this.parcelasTotal,
    this.parcelasPagas, this.conta, this.description});

//  Register();

  Register.fromMap(Map map){
    id = map[idColumn];
    value = map[valueColumn];
    isSaldo = map[isSaldoColumn];
    parcelasTotal = map[parcelasTotalColumn];
    parcelasPagas = map[parcelasPagasColumn];
    conta = map[contaColumn];
    description = map[descriptionColumn];
  }

  Map toMap() {
    Map<String, dynamic> map = {
      valueColumn: value,
      isSaldoColumn: isSaldo,
      parcelasTotalColumn: parcelasTotal,
      parcelasPagasColumn: parcelasPagas,
      contaColumn: conta,
      descriptionColumn: description
    };
    if(id != null){
      map[idColumn] = id;
    }
    return map;
  }

  @override
  String toString() {
    return "Register(id: $id, value: $value, isSaldo: $isSaldo, parcelas total: $parcelasTotal, parcelas pagas: $parcelasPagas, conta: $conta, description: $description)";
  }

}
