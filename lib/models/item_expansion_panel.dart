import 'package:flutter/material.dart';
import 'package:poup/models/item_of_lateral_list.dart';
import 'package:poup/util/sqf_helper.dart';

//CLASSE DOS ITEMS EXPANDED
class ItemExpansionPanel extends StatefulWidget {
  const ItemExpansionPanel(
      this.registers,
      {
        Key key,
        this.child,
      }) : super(key: key);

  final List<Register> registers;
  final Widget child;

  _ItemExpansionPanelState createState() => _ItemExpansionPanelState();
}

//CLASSE DO STATE DOS ITEMS EXPANDED
class _ItemExpansionPanelState extends State<ItemExpansionPanel> {

  bool isExpanded = false;
//  double _bodyHeight= 0.0;
  double _bodyHeight= 120.0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Card(
          child: _buildPanelHeader(),
        ),
        new Card(
          child: AnimatedContainer(
            child: _buildPainelExpanded(),
            curve: Curves.easeInOut,
            duration: const Duration(milliseconds: 500),
            height: _bodyHeight,
            // color: Colors.red,
          ),
        ),
      ],
    );
  }

  _buildPanelHeader() {
    return GestureDetector(
      onTap: (){
        setState((){
          if(!isExpanded){
            _bodyHeight=120.0;
            isExpanded = true;
          }else{
            _bodyHeight =0.0;
            isExpanded = false;
          }
        });
      },
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
        leading: Container(
          padding: EdgeInsets.only(right: 12.0),
          decoration: BoxDecoration(
              border: Border(right: BorderSide(width: 1.0, color: Colors.white))),
          child: Icon(Icons.add, color: Colors.white),
        ),
        title: Text(
          "Ultimos X dias",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  _buildPainelExpanded() {
    return Container(
//      color: Colors.blueGrey[400],
      color: Colors.black,
      alignment: Alignment.topLeft,
      child: ListView.builder(
          reverse: true,
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemCount: widget.registers.length,
          itemBuilder: (BuildContext context, int index) => GestureDetector(
            onTap: () {_buildBottomSheetDetailOfRegister(widget.registers[index]);},
//            child: _builItemLateralList(index),
            child: ItemOfLateralList(widget.registers[index]),
          )
      ),
    );
  }

  _buildBottomSheetDetailOfRegister(Register register) {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: 300.0,
            color: Colors.blueGrey[100],
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                        child: GestureDetector(
                            onTap: () {},
                            child: Padding(
                              padding: EdgeInsets.all(20.0),
                              child: Text(
                                "Editar",
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontWeight: FontWeight.bold),
                              ),
                            ))),
                    Expanded(
                        child: GestureDetector(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Padding(
                              padding: EdgeInsets.all(20.0),
                              child: Text(
                                "Fechar",
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    color: Colors.red,
                                    fontWeight: FontWeight.bold),
                              ),
                            ))),
                  ],
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding:
                      EdgeInsets.only(right: 30.0, left: 15.0, top: 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.remove,
                            color: Colors.red,
                            size: 40.0,
                          ),
                          Text(
                            "R\$200,00",
                            style: TextStyle(
                                fontSize: 36.0,
                                color: Colors.red,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(1.0),
                  child: Text(register.conta,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold)),
                ),
                Padding(
                  padding: EdgeInsets.all(1.0),
                  child: Text("(3 de 4)",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold)),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 1.0, bottom: 30),
                  child: Text("(R\$600,00 de R\$800,00)",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold)),
                ),
              ],
            ),
          );
        });
  }
}