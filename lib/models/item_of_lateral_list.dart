import 'package:flutter/material.dart';
import 'package:poup/util/sqf_helper.dart';

class ItemOfLateralList extends StatelessWidget {

  ItemOfLateralList(this.register);

  final Register register;

  @override
  Widget build(BuildContext context) {
    return Card(
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5.0),
                gradient: LinearGradient(colors: [
                  Color.fromARGB(255, 69, 39, 160),
                  Color.fromARGB(90, 69, 39, 160)
                ], begin: Alignment.topLeft, end: Alignment.bottomRight)
            ),
            width: 120.0,
            height: 120.0,
            padding: EdgeInsets.all(2.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                    "(${register.parcelasPagas.toString()}/${register.parcelasTotal.toString()})",
                    style: TextStyle(color: Colors.white)),
                Padding(padding: EdgeInsets.only(bottom: 20.0, top: 20.0), child: _buildValueWidget(),),
                Text(register.id.toString(), style: TextStyle(color: Colors.white)),
              ],
            )
        )
    );
  }

  _buildValueWidget() {
    if (register.isSaldo == 1) //true
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("+${register.value}",
              style: TextStyle(color: Colors.green, fontSize: 23.0))
        ],
      );
    else //false
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("- ${register.value}",
              style: TextStyle(color: Colors.red, fontSize: 23.0))
        ],
      );
  }
}